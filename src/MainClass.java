import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;


public class MainClass {
    private static final String FILE_PATH="https://arxiv.org/pdf/1701.08123.pdf";
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ExecutorService pool=Executors.newSingleThreadExecutor();
		Future<DownloadInfo> futureInfo=pool.submit(new DownloadTask(FILE_PATH));
		pool.shutdown();
		try {
			if(pool.awaitTermination(30, TimeUnit.SECONDS)){
				System.out.println("Plik o rozmiarze "+futureInfo.get().getFileSize()+" bajtów zapisano na dysku");
			}
			else
			{
				System.out.println("Operacja nie zakończyła się przed upłynięciem limitu czasu");
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
