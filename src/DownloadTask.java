import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.concurrent.Callable;

public class DownloadTask implements Callable<DownloadInfo>{
	private String path;
    public DownloadTask(String path) {
	this.path=path;
	}
	@Override
	public DownloadInfo call() throws Exception {
		long fileSize=0;
		URL url=new URL(path);
		try(InputStream is=url.openStream();
				FileOutputStream fos=new FileOutputStream(extractFileName())){
			int bufSize=2048,c;
			byte[] buffer=new byte[bufSize];
			while((c=is.read(buffer, 0, bufSize))>-1){
				fos.write(buffer, 0, c);
				fileSize+=c;
			}
		}
		return new DownloadInfo(fileSize);
	}

	private String extractFileName(){
		return path.substring(path.lastIndexOf("/")+1);
	}
}
